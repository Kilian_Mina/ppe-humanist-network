﻿# PPE : Humanist Network

Proposé par le BTS SIO du lycée Bellepierre, HNT est une ONG fictive qui a pour but de développer l'informatique dans les milieux caritatifs et humanitaires.
« Stimuler l'activité des associations caritatives par le numérique, c'était l'idée développée par les responsables de Médecins du monde , Greenpeace , Unicef et Plan International »

Proposé par le BTS SIO du lycée Bellepierre, HNT est une ONG fictive qui a pour but de développer l'informatique dans les milieux caritatifs et humanitaires.

« Stimuler l'activité des associations caritatives par le numérique, c'était l'idée développée par les responsables de Médecins du monde , Greenpeace , Unicef et Plan International »




## Activité 1 ##

	Reprise du site commencé par un stagiaire: 
		- Le design, les balise Méta, la compatibilité est à retravailler
		- Rajout de page, d'un formulaire (JavaScript obligatoire)
	
	Version V0.0 / V0.4

## Activité 2 ##

	Dynamisation du site et ajout d'une base de donnée (MySQL)
		-footer, formulaire, authentification, page statut, 
		-dynamisation de l'ajout, modification, suppression des membres
	
	Version V1.0 / V1.2 / V1.3 / V1.3.1 / V1.3.2 / V1.3.3

## Activité 3 ##

	Amélioration du site web (page à rendre dynamique)
		- Les pages News
		- La liste des membres (si Activité deux non faite)
		- l'historique
		- les liens utiles
		
	Version V1.4 / V1.4.1 / V1.4.2 / V1.4.3 / V1.4.4 / V1.4.5 / V1.5 / V1.5.1 / V1.5.2

## Activité 4 ##

	Implémentation de la technologie AJAX (liste déroulante auto)
	Restructuration par le modèle MVC
	
	Version V1.6 / V1.7 / V1.8 / V1.9 / V1.9.1

