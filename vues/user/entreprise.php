<div class="dashboard">
    <div class="row">

<a href="index.php?uc=usr_historic">
        <div class="col l4 m6 s12">
            <div class="card blue darken-3 white-text valign-wrapper">
                <div class="blue darken-3 icon valign-wrapper">
                    <i class="material-icons medium valign">&nbsp;language</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <h3 class="card-stats-number">Historique</h3>
                    </div>
                </div>
            </div>
        </div></a>
		<a href="index.php?uc=usr_statut">
        <div class="col l4 m6 s12">
            <div class="card  orange white-text valign-wrapper">
                <div class="orange icon valign-wrapper">
                    <i class="material-icons medium valign">&nbsp;timeline</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <h3 class="card-stats-number">Statut</h3>
                    </div>
                </div>
            </div>
        </div></a>
		<a href="index.php?uc=usr_geographic">
        <div class="col l4 m6 s12">
            <div class="card teal darken-3 white-text valign-wrapper">
                <div class=" teal darken-3 icon valign-wrapper">
                    <i class="material-icons medium valign">&nbsp;place</i>
                </div>
                <div class="card-content">
                    <div class="row">
                        <h3 class="card-stats-number">Géographie</h3>
                    </div>
                </div>
            </div>
        </div></a>
		 </div>
		  </div>