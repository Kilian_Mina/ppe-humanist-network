 <div  class="container col l10 s10 m10" align="center" >
<div id="corps">
	<h1>Humanist Network</h1>
	<h4>Humanist Network (HNT)  est une société de service (SAS) </h4>
	<br />
	<h3>S T A T U T S </h3>
	<br />
	
	
	
	   <table class="striped">
        <thead>
          
              <th>CARACTERISTIQUES JURIDIQUES</th>
            
          
        </thead>

        <tbody>
          <tr>
            <td>CONDITIONS DE FOND</td>
            <td>Librement fixé par les associés 
						   Libéré de moitié
						   Interdiction de l'appel public à l'épargne.
						   Possibilité d'émission privée d'obligations. </td>
           
          </tr>
          <tr>
            <td>DROITS SOCIAUX</td>
            <td>Action nominative
				Virement de compte à compte.
				Possibilité de clauses de stabilité et de contrôle de l'actionnariat :
				-inaliénabilité
				-agrément pour toutes les cessions
				-préemption
				-exclusion.</td>
           
          </tr>
          <tr>
            <td>Gestion</td>
            <td>Président qui peut être une personne morale non actionnaire.
				Totale liberté pour fixer la composition de l'organe de gestion.</td>
           
          </tr>
		  	   
		<tr>
			<td>Responsabilité</td>
			<td>
				Pour les dirigeants.
				Responsabilité civile individuelle pour propre faute.
				Responsabilité civile collective et solidaire si faute de gestion.
				Responsabilité pénale.
				Responsabilité commerciale si faillite.
			</td>
		</tr>    
		   
        </tbody>
      </table>
	
	
	
	
	
	
	
	
	
	<br>
	
	
	
	
	</div>
	

</div>