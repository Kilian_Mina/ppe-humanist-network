<?php

session_start();
include("../../modeles/hntbdd.php");
$thePdo = hnt::getPdohnt();




$page = getenv("QUERY_STRING");


if ($page == "ajouter") {
    $Prenom = $_POST['Prenom'];
    $nom = $_POST['Nom'];
    $email = $_POST['Email'];
    $tel = $_POST['Tel'];
    $qualif = $_POST['Qualif'];
    $lien = $_FILES['img_membre']['name'];

    $chemin_destination = '../../doc/img/membres/';
    move_uploaded_file($_FILES['img_membre']['tmp_name'], $chemin_destination . $_FILES['img_membre']['name']);

    $thePdo->ajoutermembres($Prenom, $nom, $email, $tel, $qualif, $lien);

    echo "Ajout fait ... Redirection en cours ...";
    echo "<script type='text/javascript'>document.location.replace('../../index.php?me=admin_menu&uc=adm_member_list');</script>";
}

if ($page == "modifier") {


	$Idmembre = $_POST['Idmembre'];
	$Prenom = $_POST['Prenom'];
	$nom = $_POST['Nom'];
	$email = $_POST['Email'];
	$tel = $_POST['Tel'];
	$qualif = $_POST['Qualif'];
   

    
    $thePdo->modifiermembres($Idmembre, $Prenom, $nom, $email, $tel, $qualif);
    echo "Modification faite ... Redirection en cours ...";

    echo "<script type='text/javascript'>document.location.replace('../../index.php?me=admin_menu&uc=adm_member_list');</script>";
}

if ($page == "supprimer") {

    $ref = $_POST['refNomPrenom'];
    $ref = explode(" ", $ref);

    $nom = $ref[0];
    $Prenom = $ref[1];


    $Supprimer = $thePdo->supprimermembres($Prenom, $nom);
    echo "Suppression faite ... Redirection en cours ...";
    echo "<script type='text/javascript'>document.location.replace('../../index.php?me=admin_menu&uc=adm_member_list');</script>";
}
?>
