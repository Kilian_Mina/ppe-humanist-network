<?php

session_start();
include("../../modeles/hntbdd.php");
$thePdo = hnt::getPdohnt();

$page = getenv("QUERY_STRING");

if ($page == "modifier") {

    $NumN = $_POST['NoN'];
    $NomN = $_POST['Tire_modif_news'];
    $Source = $_POST['Source_modif_news'];
    $TxtN = $_POST['textenews'];

    $dateModif = date("Y-m-d");

    $Img = $_POST['Img'];
    $lien = $lien = $_FILES['imagemodifnews']['name'];

    if (isset($lien)) {
        $lien = $Img;
    } else {
        $lien = $_FILES['imagemodifnews']['name'];
        $chemin_destination = '../../doc/img/news/';
        move_uploaded_file($_FILES['imagemodifnews']['tmp_name'], $chemin_destination . $_FILES['fichier']['name']);
    }


    $thePdo->modifnews($NomN, $Source, $lien, $TxtN, $dateModif, $NumN);

    echo "Modification faite ... Redirection en cours ...";
    echo "<script type='text/javascript'>document.location.replace('../../index.php?me=admin_menu&uc=adm_accueil');</script>";
}


if ($page == "ajouter") {

    $NomN = $_POST['titrenews'];
    $Source = $_POST['sourcenews'];
    $TxtN = $_POST['textenews'];

    $Date = date("Y-m-d");

    $lien = $_FILES['imagenews']['name'];

    $chemin_destination = '../../doc/img/news/';
    move_uploaded_file($_FILES['imagenews']['tmp_name'], $chemin_destination . $_FILES['imagenews']['name']);


    $thePdo->ajoutnews($NomN, $Source, $Date, $lien, $TxtN);

    echo "Ajout fait ... Redirection en cours ...";
    echo "<script type='text/javascript'>document.location.replace('../../index.php?me=admin_menu&uc=adm_accueil');</script>";
}
?>
