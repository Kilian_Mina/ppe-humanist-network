<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">Identifiez-vous </a>
							</div>
							<div class="col-xs-6">
								
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="vues/all/v_authentification.php" method="get" role="form" style="display: block;">
									<div class="form-group">
										<input type="email" name="text" id="email" tabindex="1" class="form-control" placeholder="Addresse Mail" value="">
									</div>
									<div class="form-group">
										<input type="password" name="mdp" id="password" tabindex="2" class="form-control" placeholder="Mot de passe">
									</div>
								
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Se connecter !">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="register-form" action="vues/all/v_authentification.php" method="post" role="form" style="display: none;">
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Nom" value="">
									</div>
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Prenom" value="">
									</div>
									<div class="form-group">
										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Addresse mail" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Mot de passe">
									</div>
									<div class="form-group">
										<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirmez votre mot de passe">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Enregistrez vous !">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



<script>
	$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(500).fadeIn(500);
 		$("#register-form").fadeOut(500);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(500).fadeIn(500);
 		$("#login-form").fadeOut(500);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});

</script>
















<!--


<div class="row">
	<div class="col-xs-6 col-md-4" style="margin-top:10%;">
		<form class="form-inline" action="vues/all/v_authentification.php" method="GET">
		  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
			<div class="input-group-addon">@</div>
			<input type="text" name="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="toto@titi.to" >
		  </div>
			<label class="sr-only" >Mot de Passe</label>
			<div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-addon">Mdp</div>
				<input type="password" name="mdp" class="form-control" placeholder="toto">
			</div>
			
			<div class="form-check mb-2 mr-sm-2 mb-sm-0">
				<label class="form-check-label">
					
					<button type="submit" class="btn btn-primary">Valider</button>
				</label>
			</div>
		  
			
		</form>
	</div>
	
	<div class="col-xs-6 col-md-4" style="padding-right: 80px;">
		<h3>
			Connecter vous ou inscrivez vous !! 
		</h3>
	</div>
	
	<div class="col-xs-6 col-md-4" style="margin-top:10px;width: 20%;">
		<form method= "get" name="form" id="formuaire" >
			
			<div class="form-group row">
				<label class="col-2 col-form-label">Nom*</label>
				<div class="col-10">
					<input class="form-control" type="text" id="example-text-input1">
				</div>
			</div>
				
			<div class="form-group row">
				<label  class="col-2 col-form-label">Prénom*</label>
				<div class="col-10">
					<input class="form-control" type="text" id="example-search-input2">
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-2 col-form-label">E-mail*</label>
				<div class="col-10">
					<input class="form-control" type="email" id="example-email-input3">
				</div>
			</div>
		
			<div class="form-group row">
				<label  class="col-2 col-form-label">Telephone*</label>
				<div class="col-10">
				  <input class="form-control" type="tel" id="example-tel-input">
				</div>
			</div>
			
			<div class="form-group row">
				<label  class="col-2 col-form-label">Adresse*</label>
				<div class="col-10">
					<input class="form-control" type="text" id="example-text-input4">
				</div>
			</div>
			
			<div class="form-group row">
				<label  class="col-2 col-form-label">Mot de passe*</label>
				<div class="col-10">
				  <input class="form-control" type="password"  id="example-password-input1">
				</div>
			</div>
			
			<div class="form-group row">
				<label  class="col-2 col-form-label">Confirmer le mot de passe*</label>
				<div class="col-10">
				  <input class="form-control" type="password"  id="example-password-input2">
				</div>
			</div>
			
			<div class="form-check">
				<label class="form-check-label">
					<input type="checkbox" class="form-check-input">
					coche moi pour accepter <a href="doc/pdf/cgu-hnt.pdf">les conditions générale</a> de de notre site !!
				</label>
			</div>
			
			<button type="submit" class="btn btn-primary">Valider</button>
			<h6>* est obligatoire</h6>
		</form>
	</div>
</div>

-->












