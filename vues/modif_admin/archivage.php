<?php

require('../../fpdf/fpdf.php');
include("../../modeles/hntbdd.php");
$thePdo = hnt::getPdohnt();

class PDF extends FPDF {

// en-tête
    function Header() {
        //Police Arial gras 15
        $this->SetFont('Arial', 'B', 14);
        //Décalage à droite
        $this->Cell(80);
        //Titre
        $this->Cell(30, 10, 'Mon joli fichier PDF', 0, 0, 'C');
        //Saut de ligne
        $this->Ln(20);
    }

// pied de page
    function Footer() {
        //Positionnement à 1,5 cm du bas
        $this->SetY(-15);
        //Police Arial italique 8
        $this->SetFont('Arial', 'I', 8);
        //Numéro de page
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

// création du pdf
$pdf = new PDF();
$pdf->SetAuthor('Un grand écrivain');
$pdf->SetTitle('Mon joli fichier');
$pdf->SetSubject('Un exemple de création de fichier PDF');
$pdf->SetCreator('fpdf_cybermonde_org');
$pdf->AliasNBPages();
$pdf->AddPage();


$sql = $thePdo->archivagetest();


while ($data = $sql->fetch()) {
    $id = $data["NoN"];
    $titre = $data["NomN"];
    $editeur = $data["EditeurN"];
    $date = $data['DateN'];
    $texte = $data['TextInfoN'];
    // titre en gras
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Write(5, 'News n° ' . $id . ' : ' . $titre);
    $pdf->Ln();
    // editeur
    $pdf->SetFont('Arial', '', 10);
    $pdf->Write(3, "par " . $editeur);
    $pdf->Ln();
    //date
    $pdf->SetFont('Arial', '', 10);
    $pdf->Write(3, $date);
    $pdf->Ln();
    $pdf->Ln();
    //texte
    $pdf->SetFont('Arial', '', 10);
    $pdf->Write(3, $texte);
    $pdf->Ln();
}

$pdf->Output('../../doc/archivage/pdftest1.pdf','F' );

?>