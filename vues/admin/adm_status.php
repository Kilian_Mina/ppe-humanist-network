<?php
if ($_SESSION['modadmin'] != "admin") {
    echo "<script type='text/javascript'>document.location.replace('index.php?uc=usr_form');</script>";
}
?> 
<div id="corps">
	<h1>Humanist Network</h1>
	<h3>Humanist Network (HNT)  est une société de service (SAS) </h3>
	<br />
	<h2>S T A T U T S </h2>
	<br />
	CARACTERISTIQUES JURIDIQUES
	
	<table class="table-bordered">
	   
		<tr>
			<td>CONDITIONS DE FOND</td>
			<td>Librement fixé par les associés 
						   Libéré de moitié
						   Interdiction de l'appel public à l'épargne.
						   Possibilité d'émission privée d'obligations. </td>
		</tr>
			   
		<tr>
			<td>DROITS SOCIAUX</td>
			<td>
				Action nominative
				Virement de compte à compte.
				Possibilité de clauses de stabilité et de contrôle de l'actionnariat :
				-inaliénabilité
				-agrément pour toutes les cessions
				-préemption
				-exclusion.
			</td>
		</tr>    
			
			
		<tr>
			<td>Gestion</td>
			<td>
				Président qui peut être une personne morale non actionnaire.
				Totale liberté pour fixer la composition de l'organe de gestion.
			</td>
		</tr>    
				   
		   
		<tr>
			<td>Responsabilité</td>
			<td>
				Pour les dirigeants.
				Responsabilité civile individuelle pour propre faute.
				Responsabilité civile collective et solidaire si faute de gestion.
				Responsabilité pénale.
				Responsabilité commerciale si faillite.
			</td>
		</tr>    
		   
			   
	</table>
	   
	   
</div>