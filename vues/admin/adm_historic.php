<?php
if ($_SESSION['modadmin'] != "admin") {
    echo "<script type='text/javascript'>document.location.replace('index.php?uc=usr_form');</script>";
}
?>

<div id="corps">
	<div id="div">
		<h2 style="align-content: center;">L'Entreprise</h2>
		<hr >
 
		<h3 style="align-content: center;" >Historique</h3>
		
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<em class="text-center">Créée le 31 juillet 1992 par 4 membres appartenant à des Organismes Non Gouvernementaux (ONG) 
				elle a pour but de développer l'informatique dans les milieux caritatifs et humanitaires. </em><br/><br/>
				 
				<em class="text-center">« Stimuler l'activité des associations caritatives par le numérique, c'était l'idée développée par 
				les responsables de Médecins du monde , Greenpeace , Unicef et Plan International »</em><br/><br/>
				 
				<em class="text-center">HNT possède des bureaux à proximité des centres administratifs de ses principaux clients : Paris, 
				Londres, Toronto et Madrid. Son siège social est à L'ile de la Réunion</em><br/>
				<br/><br/>
			</div>
		</div>
		
		<h3 style="align-content: center;">Présidents successifs</h3>
		
		
		<table class="table-bordered" >
			<tr>
				<td><p>Années</p></td> <td><p>Présidents</p></td>
			</tr>
			<tr>
				<td><p>1992 - 2000</p></td> <td><p>Jean-Claude TEUXCHAN</p></td>
			</tr>
			<tr>
				<td><p>2001 - 2002 </p></td> <td><p>Nicolas FAYDE</p></td>
			</tr>
			<tr>
				<td><p>2002 - 2005</p></td> <td><p>Christelle THIEUMA</p></td>
			</tr>
			<tr>
				<td><p>2005 - 2009</p></td> <td><p>Kévin GNAPACHAMPS</p></td>
			</tr>
			<tr>
				<td><p>2009 ...</p></td> <td><p>Jean-Perre LEMILLIARD</p></td>
			</tr>
		</table>
		<br />
		
		<h3 style="align-content: center;"  >Évolution de ses effectifs</h3>
		<table class="table-bordered" >
			<tr>
				<td><p>Année</p></td>
				<td><p>1992</p></td>
				<td><p>1994</p></td>
				<td><p>2000</p></td>
				<td><p>2007</p></td>
				<td><p>2010</p></td>
				<td><p>2011</p></td>
				<td><p>2012</p></td>
			</tr>
			
			<tr>
				<td><p>Effectif</p></td>
				<td><p>4   </p></td>
				<td><p>6   </p></td>
				<td><p>15  </p></td>
				<td><p>20  </p></td>
				<td><p>60  </p></td>
				<td><p>100 </p></td>
				<td><p>95  </p></td> 
			</tr>
				
		</table>
		   
		 
	</div>
</div>