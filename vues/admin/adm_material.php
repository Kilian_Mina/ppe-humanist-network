<?php
if ($_SESSION['modadmin'] != "admin") {
    echo "<script type='text/javascript'>document.location.replace('index.php?uc=usr_form');</script>";
}
?>

<div id="corps">
               <h1>Nos Services:</h1>

               <br />

               <table class="table-bordered">

	               <tr>
		               <td><u><strong>Orientés matériel :</strong></u></td>
		               <td><u><strong>Orientés développement :</strong></u></td>
	               </tr>
  
	               <tr>
		               <td>Installation de matériel,câblage de lieux.</td>
		               <td> Développement d’applications Windows sur mesure en Visual Basic, Delphi, Windev.</td>
	               </tr>	
	               
	               <tr>
		               <td>Sécurisation des lieux informatiques, paramétrage des systèmes clients et serveurs.
		               <td>Développement de sites web sur mesure : Html, CSS, Javascript, Ajax, Php, mySql, Joomla</td>
	               </tr>	

               </table>
</div>