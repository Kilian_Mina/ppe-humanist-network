<?php
if ($_SESSION['modadmin'] != "admin") {
    echo "<script type='text/javascript'>document.location.replace('index.php?uc=usr_form');</script>";
}
?>

<script>
	function ajout(){
        document.location.href="index.php?me=admin_menu&uc=ajout_membre_list";
    }
    function modif(IdMembres){
        document.location.href="index.php?me=admin_menu&uc=modif_membre_list&membre="+IdMembres;
    }
    function suppr(IdMembres){
        document.location.href="index.php?me=admin_menu&uc=suppr_membre_list&membre="+IdMembres;
    }
    function sup(IdMembres){
        rep = confirm('Etes-vous sûr de vouloir supprimer ce membre ?');
     
        if(rep){ // Si la personne confirme la suppresion
            document.location.href="index.php?uc=suppr_membre_list&membre="+IdMembres;
        }
        else{ // Si la personne annule ça demande de suppresion
            return false;
        }              
    }
</script>


<?php
$commentaire = $thePdo->affichemembres();

?>

<div class="article">
 <div class="section white">
    <div class="row container">
        <?php
        while ($donnees = $commentaire->fetch()) {
            ?>
            <div class="col  m4 s12 " align="center">
                <img id="img_membre" class="z-depth-5"   width="120px" height="140px" src='<?php echo "doc/img/membres/" . $donnees['lienmbr']; ?>' alt="<?php echo $donnees['lienmbr']; ?>" >
                <h5 class="header " align="center"><?php echo $donnees['Nommbr'] . " " . $donnees['Prenommbr']; ?></h5>
                <h6  align="center"> <?php echo $donnees['Qualifmbr']; ?><br>
                 <?php echo $donnees['emailmbr']; ?><br>
                 <?php echo $donnees['telmbr']; ?></h6>

                 <a  class="btn-floating btn-large  z-depth-5 waves-effect waves-light" onclick=modif(<?php echo $donnees['IdMembres']; ?>) ><i class="material-icons">mode_edit</i></a>
                 <button type="button" class="btn btn-primary btn-lg red  z-depth-5" name="sup" onclick="sup(<?php echo $donnees['IdMembres']; ?>)" >Supprimer un membre</button><br><br><br><br>
             </div>
             <?php 
         }
         ?>
     </div>
 </div> 
</div>

<section class="top-bar-section" id="mean_nav">
    <ul>
        <div class="fixed-action-btn horizontal ">

            <li><a class="btn-floating btn-large orange darken-3 z-depth-5 waves-effect waves-light pulse" onclick=ajout() href="#pru"><i class="material-icons">add</i></a></li>
        </div>
    </ul>
</section>