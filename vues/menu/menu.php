 <nav class="nav-extended teal lighten-2 ">
    <div class="nav-wrapper ">
      <a href="#" class="brand-logo">HNT</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="index.php" >Accueil</a></li>
        <li><a href="index.php?uc=entreprise" >Entreprise</a></li>
        <li><a href="index.php?uc=usr_form">Connexion</a></li>
        <li><a href="index.php?uc=usr_doc" >Nos Solutions</a></li>
        <li><a href="index.php?uc=usr_member_list" >Membres</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php" >Accueil</a></li>
        <li><a href="index.php?uc=entreprise" >Entreprise</a></li>
        <li><a href="index.php?uc=usr_form">Connexion</a></li>
        <li><a href="index.php?uc=usr_doc" >Nos Solutions</a></li>
        <li><a href="index.php?uc=usr_member_list" >Membres</a></li>
      </ul>
    </div>
    
  </nav>
