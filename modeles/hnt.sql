-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 22 Mai 2017 à 09:04
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `hnt`
--
CREATE DATABASE IF NOT EXISTS `hnt` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hnt`;

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `IdAdmin` int(11) NOT NULL AUTO_INCREMENT,
  `Login` text NOT NULL,
  `Mdp` text NOT NULL,
  PRIMARY KEY (`IdAdmin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `archivage`
--

DROP TABLE IF EXISTS `archivage`;
CREATE TABLE IF NOT EXISTS `archivage` (
  `NoArch` int(11) NOT NULL AUTO_INCREMENT,
  `DateArch` date NOT NULL,
  `NomPdf` varchar(50) NOT NULL,
  PRIMARY KEY (`NoArch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `mandater`
--

DROP TABLE IF EXISTS `mandater`;
CREATE TABLE IF NOT EXISTS `mandater` (
  `id_M` int(11) NOT NULL,
  `idMembres` int(11) NOT NULL,
  `DteE` date NOT NULL,
  PRIMARY KEY (`idMembres`),
  KEY `idMembres` (`idMembres`),
  KEY `DteE` (`DteE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `mandater`
--

INSERT INTO `mandater` (`id_M`, `idMembres`, `DteE`) VALUES
(3, 2, '2002-01-01'),
(1, 3, '1992-01-01'),
(2, 4, '2000-01-01'),
(4, 9, '2005-01-01'),
(5, 12, '2009-01-01'),
(0, 15, '2012-10-16');

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

DROP TABLE IF EXISTS `membres`;
CREATE TABLE IF NOT EXISTS `membres` (
  `IdMembres` int(11) NOT NULL AUTO_INCREMENT,
  `Nommbr` varchar(30) NOT NULL,
  `Prenommbr` varchar(30) NOT NULL,
  `Qualifmbr` text NOT NULL,
  `emailmbr` text NOT NULL,
  `telmbr` int(11) NOT NULL,
  `lienmbr` text NOT NULL,
  `DateFin` date DEFAULT NULL,
  PRIMARY KEY (`IdMembres`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`IdMembres`, `Nommbr`, `Prenommbr`, `Qualifmbr`, `emailmbr`, `telmbr`, `lienmbr`, `DateFin`) VALUES
(2, 'THIEUMA', 'Christelle', 'Ingénieur Réseaux', 'C.THIEUMA@HNT.COM', 262102001, 'christelle.jpg', NULL),
(3, 'TEUXCHAN', 'Jean-Claude', 'Ingénieur Etudes et Développements', 'JC.TEUXCHAN@HNT.COM', 262102002, 'robert.jpg', NULL),
(4, 'FAYDE', 'Nicolas', 'Ingénieur Etudes et Développements', 'N.FAYDE@HNT.COM', 262102003, 'leonardo.jpg', NULL),
(5, 'HUMANIA', 'Lara', 'Directrice Générale', 'L.HUMANIA@HNT.COM', 262102004, 'lara.jpg', NULL),
(6, 'LAIGLE', 'Barbara', 'Informaticienne', 'B.AIGLE@HNT.COM', 262102005, 'barbara.jpg', NULL),
(7, 'MANEQUIN', 'Adriana', 'Informaticienne', 'A.MANEQUIN@HNT.COM', 262102006, 'adriana.jpg', NULL),
(8, 'LENFANT', 'Marie', 'Directrice Informatique	', 'M.LENFANT@HNT.COM', 262102007, 'marie.jpg', NULL),
(9, 'GNAPACHAMPS', 'Kévin', 'Ingénieur Etudes et Développements', 'K.GNAPACHAMPS@HNT.COM', 262102008, 'deniro.jpg', NULL),
(10, 'MAGNUM', 'Thomas', 'Informaticien', 'T.MAGNUM@HNT.COM', 262102009, 'thomas.png', NULL),
(11, 'GHOST', 'Nicolas', 'Informaticien', 'N.GHOST@HNT.COM', 262102010, 'nicolas.jpg', NULL),
(12, 'LEMILLIARD', 'Jean-Perre', 'Président', 'JP.LEMILLIARD@HNT.COM', 262102011, 'sylvester.jpg', NULL),
(15, 'INSSA ', 'Moussa', 'Informaticien', 'moussainssa@outlook.fr', 639266702, '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `NoN` int(11) NOT NULL AUTO_INCREMENT,
  `NomN` varchar(50) NOT NULL,
  `EditeurN` varchar(50) NOT NULL,
  `DateN` date NOT NULL,
  `NomImgPrN` varchar(50) NOT NULL,
  `TextInfoN` text NOT NULL,
  `DateModif` date DEFAULT NULL,
  PRIMARY KEY (`NoN`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `news`
--

INSERT INTO `news` (`NoN`, `NomN`, `EditeurN`, `DateN`, `NomImgPrN`, `TextInfoN`, `DateModif`) VALUES
(1, 'Gabon : centre de formation pour adultes', 'Pascale HONNEUR', '2013-05-30', 'gabon.jpg', 'Informatisation d''un centre de formation pour adultes au Gabon pour l''ONG "Gabon ensemble"', NULL),
(2, 'Cambodge : Cambodge kids les nouvelles !', 'Henri BIENFAITEUR', '2013-03-05', 'cambodge.jpg', 'Informatisation de l''association "Cambodge kids"', NULL),
(3, 'Rainbow Warrior III : logiciel de gestion des dons', 'Kevin WIND', '2012-06-03', 'rw3.jpg', 'Développement d''un logiciel de gestion des dons pour la construction du Rainbow Warrior III', NULL),
(4, 'Croix Rouge : Le Sidaction, ça à du Bon !! ', 'Jean MORAUX', '2017-04-05', 'sidaction.jpg', 'La croix rouge lance une nouvelle campagne pour récolté des fonds pour pouvoir aider les zimbabouins.', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `partenaire`
--

DROP TABLE IF EXISTS `partenaire`;
CREATE TABLE IF NOT EXISTS `partenaire` (
  `idPartenaire` int(11) NOT NULL AUTO_INCREMENT,
  `Liens_P` text NOT NULL,
  `Photo_P` text NOT NULL,
  `Indic_Photo_P` text NOT NULL,
  PRIMARY KEY (`idPartenaire`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `partenaire`
--

INSERT INTO `partenaire` (`idPartenaire`, `Liens_P`, `Photo_P`, `Indic_Photo_P`) VALUES
(1, 'http://www.who.int/fr/', 'oms.jpg', 'oms'),
(2, 'http://www.greenpeace.org/france/fr/', 'greenpeace.jpg', 'greenpeace'),
(3, 'http://www.medecinsdumonde.org/', 'mdm.jpg', 'medecindumonde'),
(4, 'http://www.planfrance.org/?gclid=CMXN8_W4mqkCFQNP4QodhErpuA', 'plan.jpg', 'planfrance'),
(5, 'http://www.unicef.fr/?ls=1&utm_source=google&utm_medium=cpc&utm_term=unicef&utm_campaign=Unicef+institutionnel&gclid=CIuPgrC4mqkCFUFC4Qodmypxsw', 'unicef.jpg', 'unicef'),
(6, 'http://www.diplomatie.gouv.fr/fr/', 'francediplomatie.jpg', 'diplomatie');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `IdUser` int(11) NOT NULL AUTO_INCREMENT,
  `Nomusr` varchar(30) NOT NULL,
  `Prenomusr` varchar(30) NOT NULL,
  `emailusr` text NOT NULL,
  `telusr` int(11) NOT NULL,
  `adresseusr` text NOT NULL,
  `mdpusr` text NOT NULL,
  `statut` varchar(30) NOT NULL,
  PRIMARY KEY (`IdUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`IdUser`, `Nomusr`, `Prenomusr`, `emailusr`, `telusr`, `adresseusr`, `mdpusr`, `statut`) VALUES
(1, 'Mina', 'Kilian', 'kilian.minapro@gmail.com', 692642006, 'lycée Bellepierre', 'admin', 'Administrateur');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `mandater`
--
ALTER TABLE `mandater`
  ADD CONSTRAINT `mandater_ibfk_1` FOREIGN KEY (`idMembres`) REFERENCES `membres` (`IdMembres`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
