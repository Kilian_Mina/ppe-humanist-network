<?php

class hnt {

    private static $server = 'mysql:host=localhost';    /* <!--   Server - */
    private static $db = 'dbname=hnt';              /* <!-- Database - */
    private static $user = 'root';                    /* <!-- Username - */
    private static $pwd = '';                 /* <!-- Password - */
    private static $myPdo;
    private static $myPdoHack = null;

    /* <!-- Create a new connection - */

    private function __construct() {
        hnt::$myPdo = new PDO(hnt::$server . ';' . hnt::$db, hnt::$user, hnt::$pwd, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        /* <!-- Put UTF-8 encoding --> */
        hnt::$myPdo->query("SET character SET UTF8;");
    }

    /* <!-- Instantiate a new connection - */

    public static function getPdohnt() {
        if (hnt::$myPdoHack == null) {
            hnt::$myPdoHack = new hnt;
        }

        return hnt::$myPdoHack;
    }

    //login
    public function getLoginAdmin($Email, $Mdp) {
        $request = hnt::$myPdo->prepare("SELECT statut FROM user WHERE  emailusr = :email AND mdpusr = :mdp;");
        $request->bindParam(':email', $Email);
        $request->bindParam(':mdp', $Mdp);
        $request->execute();

        $data = $request->fetch();


        if ($data['statut'] == "Administrateur") {

            $_SESSION['modadmin'] = "admin";
            echo "<script type='text/javascript'>document.location.replace('../../index.php?me=admin_menu&uc=adm_accueil');</script>";
        }
        else if ($data['statut'] == "Utilisateur") {

            $_SESSION['modadmin'] = "user";
            echo "<script type='text/javascript'>document.location.replace('../../index.php?me=user_menu&uc=usrregis_accueil');</script>";
        }
		else{
			 echo "<script type='text/javascript'>document.location.replace('../../index.php?uc=usr_form');</script>";
		}
			
    }

    // membres
    public function affichembrmodif($idmbr) {
        $request = hnt::$myPdo->prepare("SELECT * FROM membres WHERE IdMembres = :idmembre");
        $request->bindParam(':idmembre', $idmbr);
        $request->execute();

        $data = $request->fetch();

        return $data;
    }

    public function affichemembres() {
        $request = "SELECT  *  FROM membres WHERE DateFin is NULL ORDER BY `Nommbr` ASC";

        $result2 = hnt::$myPdo->query($request);
        RETURN $result2;
    }

    public function affichememebretocombobox() {


        $request = "SELECT `Nommbr` , `Prenommbr` FROM membres WHERE DateFin is NULL ORDER BY `Nommbr` ASC";
        $result2 = hnt::$myPdo->query($request);

        while ($donnees = $result2->fetch()) {
            ?>

            <OPTION><?php echo $donnees['Nommbr'] . " " . $donnees['Prenommbr']; ?></OPTION>                   
            <?php
        }
    }

    public function recupmembre($request) {
        $result = hnt::$myPdo->query($request);
        $data = $result->fetch();
        return $data;
    }

    public function ajoutermembres($Prenom, $nom, $email, $tel, $qualif, $lien) {

        $request = hnt::$myPdo->prepare("INSERT INTO membres VALUES ('', :nom, :prenom, :qualif, :mail, :tel, '$lien',NULL);");
        $request->bindParam(':nom', $nom);
        $request->bindParam(':prenom', $Prenom);
        $request->bindParam(':qualif', $qualif);
        $request->bindParam(':mail', $email);
        $request->bindParam(':tel', $tel);
        $request->execute();
    }

    public function modifiermembres($Idmembres, $Prenom, $nom, $email, $tel, $qualif) {
        $request = hnt::$myPdo->prepare("UPDATE `membres` SET `Nommbr` = :nom, `Prenommbr` = :prenom, `emailmbr` = :mail, `telmbr` = :tel, `Qualifmbr` = :qualif  WHERE `IdMembres` = :idmbr ;");
        $request->bindParam(':idmbr', $Idmembres);
        $request->bindParam(':nom', $nom);
        $request->bindParam(':prenom', $Prenom);
        $request->bindParam(':qualif', $qualif);
        $request->bindParam(':mail', $email);
        $request->bindParam(':tel', $tel);
        $request->execute();
    }

    public function supprimermembres($IdMembres) {

		$date = date("Y-m-d");
        $request = hnt::$myPdo->prepare("UPDATE membres SET DateFin = \"".$date."\"  WHERE `IdMembres` = :idmbr  ;");
        $request->bindParam(':idmbr', $IdMembres);

        $request->execute();
    }
	


//actualite
    //affiche la news plus récente news en premier
    public function affichenews() {
        $request = "Select * from news ORDER BY NoN DESC LIMIT 5 ";
        $result = hnt::$myPdo->query($request);
       
        while ($data = $result->fetch()) {
            echo "<li>
					<img id=\"img\" src=\"doc/img/news/" . $data['NomImgPrN'] . "\" alt=\"test\"  >
                    <div class=\"caption center-align\" onclick=pagenews(" . $data['NoN'] . ")>
                        <h3>" . $data['NomN'] . "</h3>
                        <br/>
                        <h4>Posté par : " . $data['EditeurN'] . "</h4>
                    </div>
                   </li>
                ";
        }
    }

    public function Affichepagenews($NumN) {
        $request = hnt::$myPdo->prepare("select * from news where NoN = :numn");
        $request->bindParam(':numn', $NumN);
        $request->execute();
        $result = $request->fetch();
        return $result;
    }

    public function supprnews($NoN) {
        $request = hnt::$myPdo->prepare("DELETE FROM news where NoN = :numn");
        $request->bindParam(':numn', $NumN);
        $request->execute();
    }

    public function modifnews($NomN, $Source, $NomImgN, $TxtN, $dateModif, $NumN) {

        echo $NomN, $Source, $NomImgN, $TxtN, $dateModif, $NumN;

        $request = hnt::$myPdo->prepare("UPDATE news SET NomN = :nomn, EditeurN = :source, NomImgPrN = :nomimgn, TextInfoN = :txtn, DateModif = :datemodif WHERE NoN = :numn");
        $request->bindParam(':nomn', $NomN);
        $request->bindParam(':source', $Source);
        $request->bindParam(':nomimgn', $NomImgN);
        $request->bindParam(':txtn', $TxtN);
        $request->bindParam(':datemodif', $dateModif);
        $request->bindParam(':numn', $NumN);
        $request->execute();
    }

    public function ajoutnews($NomN, $Source, $Date, $NomImgN, $TxtN) {
        $request = hnt::$myPdo->prepare("INSERT INTO `news`(`NomN`, `EditeurN`, `DateN`, `NomImgPrN`, `TextInfoN`) VALUES (:NomN,:Source,:Date,:NomImgN,:TxtN)");
        $request->bindParam(':Source', $Source);
        $request->bindParam(':Date', $Date);
        $request->bindParam(':NomImgN', $NomImgN);
        $request->bindParam(':TxtN', $TxtN);
        $request->bindParam(':NomN', $NomN);
        $request->execute();
    }

    //cmd archive
    public function affichearchive() {
        $request = "select * from archive";
        $result = hnt::$myPdo->query($request);
        while ($donnees = $result->fetch()) {
            echo $donnees['NomPdf']." </br>";
        }
    }

    public function Adminaffichenews() {
        $request = "Select * from news ORDER BY NoN DESC LIMIT 5 ";
        $result = hnt::$myPdo->query($request);
        
        return $result;
    }
    
    public function AjoutArchive($NoN) {
        
    }
	public function afficheliens()
        {
			$request5 = "SELECT * FROM `partenaire` ORDER BY `idPartenaire`";
            $result5 =hnt::$myPdo->prepare($request5);
			$result5 -> execute();
			while ($donnees = $result5->fetch()){
				echo'<a href="'.$donnees['Liens_P'].'" style=" margin-right: 5px;"><img class="img-thumbnail" src="doc/img/marques/'.$donnees['Photo_P'].'" alt="'.$donnees['Indic_Photo_P'].'"/></a>';
			}
			
        }
	public function affichepresident()
        {
            $request2 = "SELECT nommbr, prenommbr, dates.DteE FROM dates INNER JOIN mandater ON dates.DteE= mandater.DteE INNER JOIN membres ON mandater.idMembres=membres.idMembres  ORDER BY `DteE` ASC";
            $result2 =hnt::$myPdo->query($request2);
			
			$request3 = "SELECT nommbr, prenommbr, dates.DteE FROM dates INNER JOIN mandater ON dates.DteE= mandater.DteE INNER JOIN membres ON mandater.idMembres=membres.idMembres  ORDER BY `DteE` ASC";
            $result3 =hnt::$myPdo->query($request3);
			$donnees2 = $result3->fetch();
			
	
			
			//$donnees2=$resultatbis -> fetch();
							
			?>
				<table class="table-bordered" >
						<tr>
							<td><p>Présidents</p></td> <td><p>Début</p></td> <td><p>Fin</p></td>
						</tr>
						<?php
						while ($donnees = $result2->fetch())
						{
							$donnees2=$result3 -> fetch();
						?>	
							<tr>
							<?php	
								
								echo "<td><p>".$donnees['prenommbr']." ".$donnees['nommbr']."</p></td> <td><p>".$donnees['DteE']."</p></td> <td><p>".$donnees2['DteE']."</p></td>";
							?>
							</tr>
						<?php
						}
						?>
				</table>
            <?php
        }
		 public function afficheeffectif()
        {
			$request3 = "SELECT * FROM `hsteffectif` ORDER BY `idHstE`";
            $result3 =hnt::$myPdo->query($request3);
            ?>
				<table class="table-bordered" >
						<tr>
							<td><p>Année</p></td> 
						<?php
						while ($donnees = $result3->fetch())
						{
						?>	
							<?php	
								echo "<td><p>".$donnees['AnneeE']."</p></td>";
							?>
						<?php
						}
						?>
						</tr>
						<tr>
							<td><p>Effectif</p></td>
						<?php
						$request4 = "SELECT * FROM `hsteffectif` ORDER BY `idHstE`";
						$result4 =hnt::$myPdo->query($request4);
						while ($donnees = $result4->fetch())
						{
						?>	
							<?php	
								echo "<td><p>".$donnees['Effectif']."</p></td>";
							?>
						<?php
						}
						?>
						</tr>
				</table>
            <?php
        }
}
?>