 <?php
 session_start();
 if (!isset($_SESSION['modadmin'])) {$_SESSION['modadmin'] = " ";}
 include("modeles/hntbdd.php");
 $thePdo = hnt::getPdohnt();
 ?>

 <!DOCTYPE html>
 <html>
	 <head charset="utf-8">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<!--Import Google Icon Font-->
		<link href="css/mat1.css" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="materialize\css/materialize.min.css"  media="screen,projection"/>
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	</head>

	<body>
	  <!--Import jQuery before materialize.js-->
	  

		<?php
		
		if ($_SESSION['modadmin'] == "admin"){
			include("vues/menu/menuadmin.php");
		}else{
			include("vues/menu/menu.php");
		} 
			
		
		?>



	<div class="slider">
	  <ul class="slides">
		<li>
		  <img src="doc/img/fond/humanist_network2.png"> 
		  <div class="caption center-align">
		  <h3>HNT</h3>
			<h5 class="light grey-text text-lighten-3">Humanist Network</h5>
		  </div>
		</li>

		<?php 
		$thePdo->affichenews();
		?> 



		</ul>
	 </div>



	 <div id="wrapper">
				<div id="sidebar-wrapper">


				   
					

				<div class="center-block">
					<div id="page-content-wrapper">
						<div class="container-fluid">
							<?php
							//Récupérer UC
							if (!isset($_REQUEST['uc']))
								$uc = 'usr_accueil';
							else

							//Récupére r UC
							if (!isset($_REQUEST['uc']))
								$uc = 'usr_accueil';
							else
								$uc = $_REQUEST['uc'];

							
							switch ($uc) {

								//all
								case 'usr_form': {
										include("vues/all/form.php");
										break;
									}
								case 'verifco': {
										include("vues/all/v_authentification.php");
										break;
									}
								case 'archivage': {
										echo "<script type='text/javascript'>document.location.replace('vues/modif_admin/archivage.php');</script>";
										break;
									}
								case 'pagearchivage': {
										include("vues/all/pagearchive.php");
										break;
									}
								case 'pagenews': {
										include ("vues/all/pagenews.php");
										break;
									}
								case 'deco': {
									$_SESSION['modadmin'] = "";
									header("Refresh:0; url=index.php");
								}

								//vues/user
								case 'usr_accueil': {
										include("vues/user/usr_accueil.php");
										break;
									}
								case 'usr_doc': {
										include("vues/user/usr_doc.php");
										break;
									}
								case 'usr_geographic': {
										include("vues/user/usr_geographic.php");
										break;
									}
								case 'usr_historic': {
										include("vues/user/usr_historic.php");
										break;
									}
								case 'usr_material': {
										include("vues/user/usr_material.php");
										break;
									}
								case 'usr_member_list': {
										include("vues/user/usr_membres_list.php");
										break;
									}
								case 'usr_statut': {
										include("vues/user/usr_status.php");
										break;
									}
								
								case 'entreprise': {
									include ("vues/user/entreprise.php");
									break;
								}


								//vues/admin
								case 'adm_accueil': {
										include("vues/admin/adm_accueil.php");
										break;
									}
								case 'adm_doc': {
										include("vues/admin/adm_doc.php");
										break;
									}
								case 'adm_geographic': {
										include("vues/admin/adm_geographic.php");
										break;
									}
								case 'adm_historic': {
										include("vues/admin/adm_historic.php");
										break;
									}
								case 'adm_material': {
										include("vues/admin/adm_material.php");
										break;
									}
								case 'adm_member_list': {
										include("vues/admin/adm_membres_list.php");
										break;
									}
								case 'adm_status': {
										include("vues/admin/adm_status.php");
										break;
									}
								case 'adm_archive': {
										include("vues/user/usr_archive.php");
										break;
									}


								//modification par l'administrateur
								case 'ajout_membre_list': {
										include("vues/modif_admin/ajout_membres_listes.php");
										break;
									}
								case 'modif_membre_list': {
										include("vues/modif_admin/modif_membres_listes.php");
										break;
									}
								case 'modif_membre_list2': {
										include("vues/modif_admin/modif_membres_listes2.php");
										break;
									}
								case 'suppr_membre_list': {
										include("vues/modif_admin/suppr_membres_listes.php");
										break;
									}
								case 'modif_news': {
										include("vues/modif_admin/modif_news.php");
										break;
									}
								case 'ajout_news': {
										include("vues/modif_admin/ajout_news.php");
										break;
									}

								//vues/userresgister
								case 'usrregis_accueil': {
										include("vues/userregister/usrregis_accueil.php");
										break;
									}
								case 'usrregis_doc': {
										include("vues/userregister/usrregis_doc.php");
										break;
									}
								case 'usrregis_geographic': {
										include("vues/userregister/usrregis_geographic.php");
										break;
									}
								case 'usrregis_historic': {
										include("vues/userregister/usrregis_historic.php");
										break;
									}
								case 'usrregis_material': {
										include("vues/userregister/usrregis_material.php");
										break;
									}
								case 'usrregis_member_list': {
										include("vues/userregister/usrregis_membres_list.php");
										break;
									}
								case 'usrregis_status': {
										include("vues/userregister/usrregis_status.php");
										break;
									}
							}

	?>
						  </div>
						</div> 
					  </div>        
					</div>




	<div class="parallax-container">
	  <div class="parallax"><img src="doc/img/fond/fond.jpg"></div>
	</div>




	<footer class="page-footer  teal lighten-2" >
	  <div class="container center">
		<?php include("vues/all/footer.php"); ?>
	  </div>
	  <div class="footer-copyright">
		<div class="container">
		  © 2014 Copyright Text
		  <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
		</div>
	  </div>
	</footer>
	<script type="text/javascript" src="js/materialize2.js"></script>
	  <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
	  <script>
		$(document).ready(function(){
		  $('.parallax').parallax();
		  $(".dropdown-button").dropdown();
		  $('.button-collapse').sideNav();
		  $('.slider').slider();
		  $('.carousel').carousel();
	 
		});
	  </script>

	</body>
</html>